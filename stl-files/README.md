Print suggestions:
    
    -   Bike Lock Mounts:
        -   Material:       PETG
        -   Layer height:   0.20 mm
        -   Infill:         20%
    
    -   Adapter Plates:
        -   Material:       PETG
        -   Layer height:   0.20 mm
        -   Infill:         100%


