# Background

This bike lock mount projects provides CAD design and STL exports for printing a u-lock holder to 
mount a Kryptonite New York Fahgettaboudit Mini u-lock to a bike frame. The mount is designed to 
attach to the bike via the water bottle mount on the down tube.

The mount is composed of two parts:

1. Bike Lock Mount
    * Bulk of the mount
    * Holds the u-lock

2. Adapter Plates
    * Provide a snug fit to the down tube of the bike
    * There are multiple adapter plates to allow for multiple bike frame shapes

This design was broken into two parts for the following reasons:
1. Printing in two parts allows to print without supports.
2. Printing in two parts allows printing the adapter plate with solid infill for a sturdier mount 
    while not increasing overall weight by needing to print the bike lock mount with solid infill.
3. If the adapter plate is not glued to the bike lock mount, you can swap the adapter plate when 
    changing bikes and reduce plastic waste.


<br/><br/>

----------------------------------------------------------------------------------------------------
## Project Components

The project is broken down into a few sub-directories:

1. <cad-files>
    * CAD design files exported from Fusion 360.

2. <docs>
    * Provides general documentation on the project (design screenshots, assembly photos, etc.).

3. <stl-files>
    * STL files that can be printed.

4. <references>
    * Reference materials used while designing the bike lock mount.


<br/><br/>

----------------------------------------------------------------------------------------------------
## Development History

For complete details on the what was changed for the latest release, please see
the [Changelog.md](Changelog.md)


<br/><br/>

----------------------------------------------------------------------------------------------------
## Licensing

All code is provided 'as is'. You are free to modify, distribute, etc. the code within the bounds
of the [Mozilla Public License (v2.0)](LICENSE.txt).


